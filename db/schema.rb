# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190408005440) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "ruc"
    t.string   "direcction"
    t.string   "sector"
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "doctor_name"
    t.string   "doctor_email"
    t.string   "billing_name"
    t.string   "billing_email"
    t.string   "description"
    t.string   "text"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "ci"
    t.string   "name"
    t.string   "lastname"
    t.string   "email"
    t.string   "phone1"
    t.string   "phone2"
    t.string   "degree"
    t.string   "workstation"
    t.string   "department"
    t.boolean  "available"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "license_type"
    t.string   "type"
    t.string   "specialty"
  end

  create_table "event_dates", force: :cascade do |t|
    t.string   "description"
    t.datetime "date"
    t.integer  "event_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "event_employees", force: :cascade do |t|
    t.string   "employee_class"
    t.integer  "employee_id"
    t.integer  "event_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "position"
  end

  create_table "event_exams", force: :cascade do |t|
    t.integer  "exam_id"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_mobile_units", force: :cascade do |t|
    t.string   "mobile_unit_class"
    t.integer  "mobile_unit_id"
    t.integer  "first_driver_id"
    t.integer  "second_driver_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "event_id"
  end

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.datetime "start"
    t.datetime "end"
    t.string   "color"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "authorization"
    t.text     "description"
    t.integer  "status"
    t.integer  "company_id"
    t.integer  "pdf_id"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
  end

  create_table "exams", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.boolean  "activated"
    t.decimal  "province_value"
    t.decimal  "m_units_value"
    t.decimal  "city_value"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["code"], name: "index_exams_on_code", using: :btree
    t.index ["name"], name: "index_exams_on_name", using: :btree
  end

  create_table "mobile_units", force: :cascade do |t|
    t.string   "plate"
    t.integer  "status"
    t.string   "company"
    t.string   "model"
    t.integer  "year"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "color"
    t.string   "type",        null: false
    t.integer  "driver_id"
    t.integer  "capacity"
    t.boolean  "rx"
    t.boolean  "flat_panel"
    t.boolean  "audiometria"
    t.boolean  "consultorio"
  end

  create_table "pdfs", force: :cascade do |t|
    t.string   "name"
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "settings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.string   "value"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role"
    t.boolean  "active"
    t.string   "avatar"
    t.string   "name"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
