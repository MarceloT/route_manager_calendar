class RemoveColumns < ActiveRecord::Migration[5.0]
  def change
  	remove_column :employees, :license_type
  	add_column :mobile_units, :driver_id, :integer
  	drop_table :event_items
  end
end
