class AddPdfIdToEvents < ActiveRecord::Migration[5.0]
  def change
  	add_column :events, :pdf_id, :integer
  end
end
