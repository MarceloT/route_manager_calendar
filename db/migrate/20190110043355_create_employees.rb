class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string :ci
      t.string :name
      t.string :lastname
      t.string :email
      t.string :phone1
      t.string :phone2
      t.string :degree
      t.string :workstation
      t.string :department
      t.boolean :available
      t.string :license_type

      t.timestamps
    end
  end
end
