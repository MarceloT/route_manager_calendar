class AddColorToMobileUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :mobile_units, :color, :string
  end
end
