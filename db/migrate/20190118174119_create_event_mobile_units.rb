class CreateEventMobileUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :event_mobile_units do |t|
      t.string :type
      t.datetime :date
      t.integer :car_id
      t.integer :first_driver_id
      t.integer :second_driver_id

      t.timestamps
    end
  end
end
