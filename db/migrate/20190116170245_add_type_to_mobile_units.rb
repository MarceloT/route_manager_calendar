class AddTypeToMobileUnits < ActiveRecord::Migration[5.0]
  def change
  	add_column :mobile_units, :type, :string, null: false
  	add_column :events, :status, :integer
  end
end
