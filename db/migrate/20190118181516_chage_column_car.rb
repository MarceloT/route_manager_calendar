class ChageColumnCar < ActiveRecord::Migration[5.0]
  def change
  	rename_column :event_mobile_units, :car_id, :mobile_unit_id
  	rename_column :event_mobile_units, :type, :mobile_unit_class
  	add_column :event_mobile_units, :event_id, :integer
  	remove_column :event_mobile_units, :date
  end
end
