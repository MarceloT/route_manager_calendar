class AddChecksToTrucks < ActiveRecord::Migration[5.0]
  def change
  	add_column :mobile_units, :rx, :boolean
  	add_column :mobile_units, :flat_panel, :boolean
  	add_column :mobile_units, :audiometria, :boolean
  	add_column :mobile_units, :consultorio, :boolean
  end
end
