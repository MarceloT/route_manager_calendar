class AddIndexToExamns < ActiveRecord::Migration[5.0]
  def change
    add_index :exams, :code
    add_index :exams, :name
  end
end
