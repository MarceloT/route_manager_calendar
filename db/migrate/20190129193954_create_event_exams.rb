class CreateEventExams < ActiveRecord::Migration[5.0]
  def change
    create_table :event_exams do |t|
      t.integer :exam_id
      t.integer :event_id

      t.timestamps
    end
  end
end
