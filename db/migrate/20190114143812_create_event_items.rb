class CreateEventItems < ActiveRecord::Migration[5.0]
  def change
    create_table :event_items do |t|
      t.integer :event_id
      t.integer :car_id
      t.integer :employee_id
      t.text :authorization

      t.timestamps
    end
  end
end
