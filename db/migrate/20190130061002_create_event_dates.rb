class CreateEventDates < ActiveRecord::Migration[5.0]
  def change
    create_table :event_dates do |t|
      t.string :description
      t.datetime :date
      t.integer :event_id

      t.timestamps
    end
  end
end
