class ChangeSettings < ActiveRecord::Migration[5.0]
  def change
  	remove_column :settings, :event_notification_email
  	add_column :settings, :name, :string
  	add_column :settings, :value, :string
  end
end
