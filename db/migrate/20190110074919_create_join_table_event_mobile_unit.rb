class CreateJoinTableEventMobileUnit < ActiveRecord::Migration[5.0]
  def change
    create_join_table :events, :mobile_units do |t|
      # t.index [:event_id, :mobile_unit_id]
      # t.index [:mobile_unit_id, :event_id]
    end
  end
end
