class AddCapacityToUnitMobile < ActiveRecord::Migration[5.0]
  def change
  	add_column :mobile_units, :capacity, :integer
  end
end
