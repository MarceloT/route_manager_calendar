class CreateMobileUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :mobile_units do |t|
      t.string :plate
      t.integer :status
      t.string :company
      t.string :model
      t.integer :year
      t.text :description

      t.timestamps
    end
  end
end
