class CreateExams < ActiveRecord::Migration[5.0]
  def change
    create_table :exams do |t|
      t.string :code
      t.string :name
      t.boolean :activated
      t.decimal :province_value
      t.decimal :m_units_value
      t.decimal :city_value

      t.timestamps
    end
  end
end
