class CreateEventEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :event_employees do |t|
      t.string :employee_class
      t.integer :employee_id
      t.integer :event_id

      t.timestamps
    end
  end
end
