class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :ruc
      t.string :direcction
      t.string :sector
      t.string :contact_name
      t.string :contact_email
      t.string :doctor_name
      t.string :doctor_email
      t.string :billing_name
      t.string :billing_email
      t.string :description
      t.string :text

      t.timestamps
    end
    add_column :events, :company_id, :integer
  end
end
