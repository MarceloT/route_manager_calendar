namespace :users do
  desc "Usuarios Iniciales"
  task :usuarios_iniciales => :environment do
    # Crear Choferes
    user = User.new
    user.email = "admin@admin.com"
    user.password = "admin@admin.com"
    user.role = 1
    user.active = true
    user.save

    Event.all.each do |e|
      e.created_by_id = user.id
      e.updated_by_id = user.id
      e.save
      e.errors.full_messages
    end

  end
end