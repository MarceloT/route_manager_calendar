namespace :tasks do
  desc "Datos Iniciales"
  task :datos_iniciales => :environment do
    # Crear Choferes
    driver = Driver.new
    driver.ci = "1749274038"
    driver.name = "Juan"
    driver.lastname = "Torres"
    driver.license_type = "b"
    driver.phone1 = "123456789"
    driver.available = true
    driver.save

    driver = Driver.new
    driver.ci = "1739403958"
    driver.name = "Pedro"
    driver.lastname = "Jara"
    driver.license_type = "c"
    driver.phone1 = "123456789"
    driver.available = true
    driver.save

    driver = Driver.new
    driver.ci = "1749883722"
    driver.name = "Mateo"
    driver.lastname = "Diaz"
    driver.license_type = "b"
    driver.phone1 = "123456789"
    driver.available = true
    driver.save

    driver = Driver.new
    driver.ci = "1748339280"
    driver.name = "David"
    driver.lastname = "Arias"
    driver.license_type = "b"
    driver.phone1 = "123456789"
    driver.available = true
    driver.save

    # Crear Empleados
    staff = Staff.new
    staff.ci = "1739584478"
    staff.name = "Mario"
    staff.lastname = "Arias"
    staff.license_type = ""
    staff.phone1 = "123456789"
    staff.available = true
    staff.save

    staff = Staff.new
    staff.ci = "1786743322"
    staff.name = "Maria"
    staff.lastname = "Jara"
    staff.license_type = ""
    staff.phone1 = "123456789"
    staff.available = true
    staff.save

    staff = Staff.new
    staff.ci = "1709878858"
    staff.name = "Jose"
    staff.lastname = "Alvarez"
    staff.license_type = ""
    staff.phone1 = "123456789"
    staff.available = true
    staff.save

    staff = Staff.new
    staff.ci = "1723334860"
    staff.name = "Ivan"
    staff.lastname = "Aguirre"
    staff.license_type = ""
    staff.phone1 = "123456789"
    staff.available = true
    staff.save

    # Crear Doctores
    doctor = Doctor.new
    doctor.ci = "1748372333"
    doctor.name = "Sandra"
    doctor.lastname = "Rosero"
    doctor.license_type = ""
    doctor.degree = "dr"
    doctor.phone1 = "123456789"
    doctor.available = true
    doctor.save

    doctor = Doctor.new
    doctor.ci = "1758392033"
    doctor.name = "Marco"
    doctor.lastname = "Gonzales"
    doctor.license_type = ""
    doctor.degree = "dr"
    doctor.phone1 = "123456789"
    doctor.available = true
    doctor.save

    doctor = Doctor.new
    doctor.ci = "1732129390"
    doctor.name = "Ariel"
    doctor.lastname = "Garcia"
    doctor.license_type = ""
    doctor.degree = "dr"
    doctor.phone1 = "123456789"
    doctor.available = true
    doctor.save

    doctor = Doctor.new
    doctor.ci = "1748939023"
    doctor.name = "Alexandra"
    doctor.lastname = "Cuello"
    doctor.license_type = ""
    doctor.degree = "dr"
    doctor.phone1 = "123456789"
    doctor.available = true
    doctor.save

    # Crear Autos
  	car = Car.new
	car.plate = "ABC1234"
    car.company = "chevrolet"
    car.model = "sail"
    car.year = "2018"
    car.color = "blanco"
    car.status = 0
    car.driver_id = "1"
	car.save

    car = Car.new
    car.plate = "ABC5647"
    car.company = "chevrolet"
    car.model = "sail"
    car.year = "2018"
    car.color = "blanco"
    car.status = 0
    car.driver_id = "2"
    car.save

    car = Car.new
    car.plate = "ABC8910"
    car.company = "chevrolet"
    car.model = "sail"
    car.year = "2018"
    car.color = "blanco"
    car.status = 0
    car.driver_id = "3"
    car.save

     # Crear Vans
    van = Van.new
    van.plate = "ABC0001"
    van.company = "suzuki"
    van.model = "sail"
    van.year = "2018"
    van.color = "blanco"
    van.status = 0
    van.driver_id = "1"
    van.save

    van = Van.new
    van.plate = "ABC0002"
    van.company = "suzuki"
    van.model = "otros"
    van.year = "2018"
    van.color = "blanco"
    van.status = 0
    van.driver_id = "2"
    van.save

    van = Van.new
    van.plate = "ABC0003"
    van.company = "suzuki"
    van.model = "otros"
    van.year = "2018"
    van.color = "blanco"
    van.status = 0
    van.driver_id = "3"
    van.save

    # Crear Truck
    truck = Truck.new
    truck.plate = "XYZ0001"
    truck.company = "suzuki"
    truck.model = "sail"
    truck.year = "2018"
    truck.color = " blanco"
    truck.status = 0
    truck.driver_id = "1"
    truck.save

    truck = Truck.new
    truck.plate = "XYZ0002"
    truck.company = "suzuki"
    truck.model = "otros"
    truck.year = "2018"
    truck.color = " blanco"
    truck.status = 0
    truck.driver_id = "2"
    truck.save

    truck = Truck.new
    truck.plate = "XYZ0003"
    truck.company = "suzuki"
    truck.model = "otros"
    truck.year = "2018"
    truck.color = " blanco"
    truck.status = 0
    truck.driver_id = "3"
    truck.save

    setting = Setting.new
    setting.name = "event_notification_email"
    setting.value = "marcelo.toapanta@hotmail.com"
    setting.save

    Event.destroy_all



   #  # CREAR USUARIOS
   #  # Usuario Administrador
  	user = User.new
  	user.email = "admin@logistica.com"
  	user.password = "admin123"
  	user.password_confirmation = "admin123"
  	user.role = :admin
  	user.save

    # profile = Profile.new
    # profile.user_id = user.id
    # profile.name = "Administrador"
    # profile.direction = "Calle 21 de Agosto E3-200, S27A"
    # profile.phone = "0995844878"
    # profile.save
    
  end
end