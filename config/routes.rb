Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  get 'pdf/index'
  post 'pdf/create'
  patch 'pdf/create'
  get 'pdf/delete'
  devise_scope :user do
    # get 'users/profil/:id', to: 'users/registrations#profil', as: 'profil'
    get 'users/registrations' => 'users/registrations#index'
    get 'users/registrations/new_user' => 'users/registrations#new_user'
    post 'users/registrations/create_user' => 'users/registrations#create_user'
    get 'users/registrations/active_user' => 'users/registrations#active_user'
    get 'users/registrations/desactive_user' => 'users/registrations#desactive_user'
    get 'users/registrations/reset_password' => 'users/registrations#reset_password'
  end


  resources :settings
  resources :exams
  resources :companies
  resources :trucks
  resources :doctors
  resources :staffs
  resources :drivers
  resources :vans
  resources :cars
  # resources :employees
  resources :events do
    collection do
      get 'add_mobile_unit'
      get 'remove_mobile_unit'
      get 'add_employee'
      get 'remove_employee'
      get 'add_exam'
      get 'remove_exam'
      get 'is_item_used'
      get 'send_confirmation_email'
    end
  end

  get '/visitors/load_data' => 'visitors#load_data'

  root 'visitors#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
