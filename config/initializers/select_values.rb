DEGREES = [
	['Sr.','sr'], 
	['Lic.','lic'], 
	['Ing.','ing'],
	['Dr.', 'dr'],
	['Otros','otros']
]
WORKSTATIONS = [
	['Recepción 1','recepcion_1'], 
	['Recepción 2','recepcion_2'], 
	['Laboratorio 1','laboratorio_1'], 
	['Laboratorio 2','laboratorio_2'],
	['Otros','otros']
]
DEPARTMENTS = [
	['Recepción','recepcion'], 
	['Sistemas','sistemas'], 
	['Finaciero','financiero'],
	['Laboratorio', 'laboratorio'],
	['Programas', 'programas'],
	['Otros','otros']
]
LICENSE_TYPES = [
	['A','a'], 
	['B','b'], 
	['C','c'], 
	['D','d'], 
	['E','e'], 
	['Otros','otros']
]

CAR_STATUS = [
	['Disponible', 0], 
	['Matenimiento',1], 
	['Fuera de servicio',2], 
	['Otros',3]
]

CAR_COLORS = [
	['Blanco','blanco'], 
	['Negro','negro'], 
	['Plomo','plomo'], 
	['Azul','azul'], 
	['Rojo','rojo'], 
	['Con publicidad','con_publicidad'], 
	['Otros','otros']
]

CAR_COMPANY = [
	['Chevrolet','chevrolet'], 
	['KIA','kia'], 
	['Mazda','mazda'], 
	['Volkswagen','volkswagen'], 
	['Suzuki','suzuki'], 
	['Nissan','nissan'],
	['Hino','hino'],
	['Hyundai','hyundai'],
	['Otros','otros'] 
]

CAR_MODEL = [
	['Onix','onix'], 
	['Versa','versa'], 
	['Gol','gol'], 
	['Hilux','hilux'], 
	['Frontier','frontier'], 
	['Sandero','sandero'], 
	['Spark','spark'],
	['Sail','sail'],
	['Corolla','corolla'],
	['Aveo','aveo'],
	['EcoSport','eco_sport'],
	['Rio','rio'],
	['Tracker','tracker'],
	['Otros','otros'] 
]

CAR_YEAR = [
	["2010", "2010"],
	["2011", "2011"],
	["2012", "2012"],
	["2013", "2013"],
	["2014", "2014"],
	["2015", "2015"],
	["2016", "2016"],
	["2017", "2017"],
	["2018", "2018"],
	["2019", "2019"],
	["Otros", "otros"],
]

CARGOS_PERSONAL = [
	["SIN ASIGNAR", ""],
	["RESPONSABLE DEL PROGRAMA", "responsable_del_programa"],
	["COORDINACIÓN", "coordinacion"],
	["INGRESO", "ingreso"],
	["EXTRACCIÓN", "extraccion"],
	["RAYOS X", "rayos_x"],
	["CAJA", "caja"],
	["ASISTENTE RADIÓLOGO", "asistente_radiologo"],
	["ROTULACIÓN", "rotulacion"],
	["AUDIOMETRÍA", "audiometria"],
	["OPTOMETRÍA", "optometría"],
	["ESPIROMETRÍA", "espirometria"],
	["ELECTROCARDIOGRAMA", "electrocardiograma"],
	["EKG - ESPIROMETRÍA", "ekg_espirometria"],
	["PAP - TEST", "pap_test"]
	
]
CARGOS_DOCTOR = [
	["SIN ASIGNAR", ""],
	["MÉDICO RADIÓLOGO", "medico_radiologo"],
	["MÉDICO GENERAL", "medico_general"],
	["MÉDICO OCUPACIONAL", "medico_ocupacional"],
	["MÉDICO INTERNISTA", "medico_internista"],
	["MÉDICO OFTALMÓLOGO", "medico_oftalmologo"],
	["MÉDICO NEUMÓLOGO", "medico_neumologo"],
	["MÉDICO GINECÓLOGO", "medico_ginecologo"],
	["MÉDICO CARDIÓLOGO", "medico_cardiologo"],
	["OPTÓMETRA", "optometra"],
	["AUDIÓLOGO", "audiologo"]
	
]
SECTOR_COMPANY = [
	["Educación","educacion"],
	["Financiero","financiero"],
	["Industrial","industrial"]
]

CAR_CAPACITY = [
	["SIN ASIGNAR", ""],
	["4","4"],
	["5","5"],
	["7","7"],
	["8","8"],
	["9","9"],
	["10","10"],
	["11","11"],
	["12","12"],
	["13","13"],
	["14","14"],
	["15","15"]
]
ROLES = [
	["Administrador","1"],
	["Recepcionista","2"],
	["Cajero","3"]
]
ROLES_USER = [
	["Recepcionista","2"],
	["Cajero","3"]
]

