EVENT_COLORS = [
	['Verde','#61bd4f'], 
	['Amarillo','#f2d600'], 
	['Naranja','#ff9f1a'], 
	['Rojo','#eb5a46'], 
	['Morado','#c377e0'], 
	['Azul','#0079bf'],
	['Gris','#cccccc']
]

# ['Borrado','0'] No disponible en dropdown
EVENT_STATUS = [
	['Planificado','1'],
	['Confirmado','2'], 
	['Suspendido','3']
]
