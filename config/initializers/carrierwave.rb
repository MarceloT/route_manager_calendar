require 'yaml'
s3_conf = YAML.load_file(Rails.root + 'config' + 's3.yml')[Rails.env]

CarrierWave.configure do |config|
  config.ignore_integrity_errors = false
  config.ignore_processing_errors = false
  config.ignore_download_errors = false
  # Rails.logger.debug "========================================= CONFIG.INSPECT"
  # Rails.logger.debug config.inspect
  if Rails.env.production? || Rails.env.staging? || Rails.env.preview? || Rails.env.development?
    config.fog_provider = 'fog/aws'                        # required
    config.fog_credentials = {
      provider:              'AWS',                        # required
      :aws_access_key_id     => s3_conf['access_key_id'],
      :aws_secret_access_key => s3_conf['secret_access_key'],
      :region                => s3_conf['region']
    }
    config.fog_directory  = s3_conf['bucket']
    config.fog_public     = true
    config.fog_attributes = { 'Cache-Control' => 'public,max-age=2592000' } # optional, defaults to {}
  elsif Rails.env.test?
    config.storage = :file
    config.enable_processing = false
  end
  config.cache_dir = "#{Rails.root}/tmp/uploads"                  # To let CarrierWave work on heroku

  # config.s3_access_policy = :public_read                          # Generate http:// urls. Defaults to :authenticated_read (https://)
  # config.fog_host         = s3_conf['host']
end