require 'test_helper'

class PdfControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get pdf_index_url
    assert_response :success
  end

  test "should get create" do
    get pdf_create_url
    assert_response :success
  end

  test "should get delete" do
    get pdf_delete_url
    assert_response :success
  end

end
