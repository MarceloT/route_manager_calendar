class EventMailer < ApplicationMailer
	def event_confirm(event)
  	@event = event
    emails = Setting.where(:name => "event_notification_email").last.value.split(";")
    mail to: emails, subject: "Programa confirmado: #{@event.title}"
  end
end
