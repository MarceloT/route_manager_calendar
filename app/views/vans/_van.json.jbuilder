json.extract! van, :id, :created_at, :updated_at
json.url van_url(van, format: :json)
