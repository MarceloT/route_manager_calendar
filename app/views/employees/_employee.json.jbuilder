json.extract! employee, :id, :ci, :name, :lastname, :email, :phone1, :phone2, :degree, :workstation, :department, :available, :license_type, :created_at, :updated_at
json.url employee_url(employee, format: :json)
