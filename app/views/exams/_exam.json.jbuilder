json.extract! exam, :id, :code, :name, :activated, :province_value, :m_units_value, :city_value, :created_at, :updated_at
json.url exam_url(exam, format: :json)
