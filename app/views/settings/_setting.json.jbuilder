json.extract! setting, :id, :event_notification_email, :created_at, :updated_at
json.url setting_url(setting, format: :json)
