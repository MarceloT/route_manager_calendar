json.extract! driver, :id, :license_type, :created_at, :updated_at
json.url driver_url(driver, format: :json)
