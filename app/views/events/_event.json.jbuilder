date_format = event.all_day_event? ? '%Y-%m-%d' : '%Y-%m-%dT%H:%M:%S'
# date_format = '%Y-%m-%d'
json.id event.id
json.title event.full_name
json.start event.start.strftime(date_format)
json.end event.end.strftime(date_format)
json.dates event.event_dates.order(:date).map{|d| "#{d.description}"}
json.color event.color unless event.color.blank?
json.allDay event.all_day_event? ? true : false

json.update_url event_path(event, method: :patch) if !@event.errors.any?
json.edit_url edit_event_path(event) if !@event.errors.any?

json.errors event.errors
