json.extract! company, :id, :name, :ruc, :direcction, :sector, :contact_name, :contact_email, :doctor_name, :doctor_email, :billing_name, :billing_email, :description, :text, :created_at, :updated_at
json.url company_url(company, format: :json)
