var initialize_calendar;

function lock_calendar(){
  console.log("lock");
  $(".calendar").addClass("lock");
  $(".lds-spinner").show();

}
function unlock_calendar(){
  console.log("unlock");
  $(".calendar").removeClass("lock"); 
  $(".lds-spinner").hide();
}

function init_select(){
  $('select.select2').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    }
  });
}

initialize_calendar = function() {
  
  $('.calendar.empty').each(function(){
    $('.calendar.empty').empty();
    var calendar = $(this);
    calendar.fullCalendar({
      header: {
        left: 'prev,next, today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      locale: 'es',
      selectable: true,
      selectHelper: true,
      editable: false,
      weekNumbers: true,
      droppable: false,
      events: '/events.json',
      eventRender: function (event, element) {
        dates = ""
        total_dates = event.dates.length
        $.each(event.dates, function( index, value ) {
          dates = dates + '<span class="event-date" style="width:'+101/total_dates+'%">'+value+'</span>'
        });
        element.find('.fc-title').after('<div class="hr-line-solid"></div>'+dates+'</div>');
      },

      select: function(start, end) {
        start = start.add(6, "hours")
        end = end.subtract(30, "minutes")
        lock_calendar();
        start_date = moment(start).format("MM/DD/YYYY HH:mm")
        end_date = moment(end).format("MM/DD/YYYY HH:mm")
        $.getScript('/events/new?start='+start_date+'&end='+end_date, function() {
          $('#event_date_range').val(moment(start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(end).format("MM/DD/YYYY HH:mm"))
          date_range_picker();
          $('.start_hidden').val(moment(start).format('YYYY-MM-DD HH:mm'));
          $('.end_hidden').val(moment(end).format('YYYY-MM-DD HH:mm'));
          unlock_calendar();
          load_upload();
          init_select();
        });

        calendar.fullCalendar('unselect');

      },

      eventDrop: function(event, delta, revertFunc) {
        console.log(event)
        event_data = { 
          event: {
            id: event.id,
            start: event.start.format(),
            end: event.end.format(),
            move: true
          }
        };
        $.ajax({
            url: event.update_url,
            data: event_data,
            type: 'PATCH'
        });
      },
      
      eventClick: function(event, jsEvent, view) {
        lock_calendar();
        $.getScript(event.edit_url, function() {
          $('#event_date_range').val(moment(event.start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(event.end).format("MM/DD/YYYY HH:mm"))
          date_range_picker();
          $('.start_hidden').val(moment(event.start).format('YYYY-MM-DD HH:mm'));
          $('.end_hidden').val(moment(event.end).format('YYYY-MM-DD HH:mm'));
          if($("#details .bs-callout").length == 0){
            $("#label-no-details").show();
          }else{
            $("#label-no-details").hide();
          }
          unlock_calendar();
          load_upload();
          init_select();
        });
      }
    });
    // calendar.removeClass("empty");
  })  
  
};
$(document).on('turbolinks:load', initialize_calendar);

