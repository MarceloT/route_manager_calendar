// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require moment
//= require fullcalendar
//= require fullcalendar/lang/es
//= require daterangepicker
//= require jquery-fileupload/basic
//= require select2/select2.full
//= require_tree .

$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });
});

$(document).on("click", "#tab-details", function(event){
  event.preventDefault();
  $("#details").show();
  $("#dates").hide();
  $(".nav.nav-tabs li").removeClass("active");
  li = $(this).parents("li");
  li.addClass("active");
});

$(document).on("click", "#tab-dates", function(event){
  event.preventDefault();
  $("#details").hide();
  $("#dates").show();
  $(".nav.nav-tabs li").removeClass("active");
  li = $(this).parents("li");
  li.addClass("active");
});


$(document).on("click", ".color-item", function(event){
		event.preventDefault();
    $("#new_event .modal-header").css("border-color", $(this).attr("color"));
    $("#edit_event .modal-header").css("border-color", $(this).attr("color"));
    $("#event_color").val($(this).attr("color"))
    
    console.log($(this).attr("color"));
});

$(document).on("click", ".dropdown-toggle", function(event){
  $(".dropdown-menu:visible").find(".text-search").val("").focus().keyup()
  
});

$(document).bind('keydown', 'ctrl+e', open_event);

function open_event(){
  $(".add-to-event").click();
}


function setSelected(menuitem) {
   $(".dropdown-menu:visible li:visible a").parents("li").removeClass("itemhover");
   $(".dropdown-menu:visible li:visible a").eq(menuitem).parents("li").addClass("itemhover");
   currentLi = $(".dropdown-menu:visible li:visible a").eq(menuitem);
}

var currentSelection = 0;
var currentLi = '';

function navigate(direction) {
   // Check if any of the menu items is selected
   if($(".dropdown-menu:visible li:visible.itemhover").size() == 0) {
      currentSelection = -1;
   }
   
   if(direction == 'up' && currentSelection != -1) {
      if(currentSelection != 0) {
         currentSelection--;
      }
   } else if (direction == 'down') {
      if(currentSelection != $(".dropdown-menu:visible li:visible").size() -1) {
         currentSelection++;
      }
   }
   console.log(currentSelection);
   setSelected(currentSelection);
}

$(document).on("keypress keydown keyup", ".text-search", function(e){
  if(e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 27) { e.preventDefault(); }
});

$(document).on("keyup", ".text-search", function(event){
  event.preventDefault();
  console.log(event.which);

  if ( event.which == 16 ) {
    console.log("event.which");
    // $(this).blur();
    // $(".btn-group.open").removeClass("open");
    $(".btn-group.open").click();
  }

  switch(event.which) {
      case 37: // left
        break;

      case 38: // up
        navigate('up');
        console.log("up");
        break;

      case 39: // right
        break;

      case 40: // down
        navigate('down');
        console.log("down");
        break;
      case 13:
        if(currentLi != '') {
          currentLi.click();
        }
        break;
  }
  console.log(currentLi);
  filter = $(this).val().toUpperCase();
  
  $('.dropdown-menu:visible li').each(function (i) {
      text = $(this).text();
      if (text.toUpperCase().indexOf(filter) > -1 || $(this).hasClass("search-item")) {
        $(this).show()
      } else {
        $(this).hide()
      }
  });

  
});

$(document).on("click", ".dropdown-menu.cars a, .dropdown-menu.vans a, .dropdown-menu.trucks a, .dropdown-menu.staffs a, .dropdown-menu.doctors a, .dropdown-menu.exams a", function(event){
  if ($(this).hasClass("redirect-open")){
  }
  else{
    event.preventDefault();
    li = $(this).parents("li");
    if (li.hasClass("active")) {
      console.log("remove item");
      li.removeClass("active");
      add_link = $(this).attr("href");
      add_link = add_link.replace("remove", "add"); 
      $(this).attr("href", add_link);
    }
    else{
      console.log("add item");
      li.addClass("active");
      remove_link = $(this).attr("href");
      remove_link = remove_link.replace("add", "remove"); 
      $(this).attr("href", remove_link);
      $(".dropdown-menu:visible").find(".text-search").focus().keyup()
    }  
  }
});

/// FILE UPLOAD

function load_upload(){
  $('#fileupload').fileupload({
    dataType: 'json',
    options: {
      maxFileSize: 5000000,
      acceptFileTypes: /(\.|\/)(pdf|docx?|txt)$/i,
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css(
          'width',
          progress + '%'
      );
    },
    add: function (e, data) {
        console.log("on add");
        console.log("data " + data);
        $('#pdf_url_new').hide();
        data.context = $('#file_name').val('Uploading...');
        data.submit();
    },
    done: function (e, data) {
        console.log("on done");
        console.log(data);
        if (data.result){
          pdf = data.result;
          $('#pdf_url_new').attr("href", pdf.file.url);
          $('#pdf_url_new').show();
          $('#event_pdf_id').val(pdf.id);
        }
        else{
          console.log("Si resultados");
        }
    }
  });
}
function edit_event(edit_url, start, end){
  
  $.getScript(edit_url, function() {
    $('#event_date_range').val(moment(start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(end).format("MM/DD/YYYY HH:mm"))
    date_range_picker();
    $('.start_hidden').val(moment(start).format('YYYY-MM-DD HH:mm'));
    $('.end_hidden').val(moment(end).format('YYYY-MM-DD HH:mm'));
    if($("#details .bs-callout").length == 0){
      $("#label-no-details").show();
    }else{
      $("#label-no-details").hide();
    }

    console.log($('#edit_event_54').attr('action'))
    load_upload();
  });
}