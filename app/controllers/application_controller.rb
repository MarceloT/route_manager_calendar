class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :gloval_values

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to root_path, alert: "No tiene permisos para acceder a esta página." }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end


  def get_data
    # Mobile uinits
    @all_cars = Car.available
    @all_vans = Van.available
    @all_trucks = Truck.available
    # Employees
    @all_drivers = Driver.available.order_by_name
    @all_staffs = Staff.available.order_by_name
    @all_doctors = Doctor.available.order_by_name
    # Events
    @all_events = Event.all
    # Companies
    @all_companies = Company.order_by_name
    # Examenes
    @all_exams = Exam.all
  end

  def gloval_values
    @breadcrumb = []
    if current_user
      @my_events = Event.where("created_by_id = #{current_user.id} AND status > 0").count
    else
      @my_events = 0
    end
  end

  def call_rake(task, options = {})
    options[:rails_env] ||= Rails.env
    args = options.map { |n, v| "#{n.to_s.upcase}='#{v}'" }
    system "cd #{Rails.root} && rake #{task} #{args.join(' ')} --trace 2>&1 >> #{Rails.root}/log/rake.log &"
  end
end
