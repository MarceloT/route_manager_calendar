# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]
  before_action :only_admin, except: [:edit, :update]
  after_action :create_profile, only: [:create]
  def create_profile
    if resource.persisted? # user is created successfuly
      profile = Profile.new
      profile.user_id = resource.id
      profile.save
    end
  end


  def index
    @users = User.all.order(:id)
  end

  def new_user
    @user = User.new
  end

  def create_user
    @user = User.new(user_params)
    @user.active = true
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_registrations_path }
        format.json { render :index, status: :created, location: users_registrations_path }
      else
        Rails.logger.info "====>> @user.errors: #{@user.errors.full_messages}"
        format.html { render :new_user }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def active_user
    @user = User.find(params[:id])
    @user.active = true
    @user.save
    Rails.logger.info "====>> @user.errors: #{@user.errors.full_messages}"
    redirect_to users_registrations_path, notice: 'Usuario activado correctamente.'
  end

  def desactive_user
    @user = User.find(params[:id])
    @user.active = false
    @user.save
    Rails.logger.info "====>> @user.errors: #{@user.errors.full_messages}"
    redirect_to users_registrations_path, notice: 'Usuario desactivado correctamente.'
  end

  def reset_password
    @user = User.find(params[:id])
    @user.password = "123456"
    @user.save
    Rails.logger.info "====>> @user.errors: #{@user.errors.full_messages}"
    redirect_to users_registrations_path, notice: 'Clave de usuario cambiada correctamente.'
  end
  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :avatar, :avatar_cache])
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  def only_admin
    unless current_user.role?(:admin)
      redirect_to root_path, alert: 'No tiene acceso a esta pagina'
    end
  end
  private

    def user_params
      params.require(:user).permit(:email, :password, :role, :name, :avatar, :avatar_cache)
    end
end
