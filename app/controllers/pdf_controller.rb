class PdfController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def create
  	@pdf = Pdf.new
  	if params[:file]
  		Rails.logger.info "====>>>>>>> on params[:file]"
  		@pdf.file = params[:file]
  	end
  	respond_to do |format|
      if @pdf.save
        format.html { }
        format.json { render json: @pdf, status: :created }
      else
        format.html { }
        format.json { render status: :unprocessable_entity, success: false }
      end
    end
  end

  def delete
  end
end
