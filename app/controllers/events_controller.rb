class EventsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :get_data, only: [:new, :edit, :create, :update, :add_mobile_unit]
  before_action :get_mobile_units, :get_employees, :get_exams, only: [:edit, :show]
  

  def index
    @events = Event.where("status > 0").order(created_at: :desc)
    if params[:start].present? && params[:end].present?
      @events = @events.where(start: params[:start]..params[:end])
    end

    # if request.html?
    # end
    respond_to do |format|
      format.html { @events = params[:all].present? ? @events : params[:deleted].present? ? Event.where("status = 0").order(:created_at) : @events.where(:created_by_id => current_user.id)}
      format.json 
    end
  end

  def show
    @breadcrumb << ["#{@event.full_name}", request.fullpath]

    respond_to do |format|
      # format.html  { render layout: false }
      format.html 
      format.pdf do
        render pdf: @event.full_name, font_size: 20, disable_links: true, lowquality: true
      end
    end
  end

  def new
    @event = Event.new
    @event.color = EVENT_COLORS.first.last
    @event.status = 1
    @event.start = DateTime.strptime(params[:start].to_s, '%m/%d/%Y')  if params[:start].present?
    @event.end = DateTime.strptime(params[:end].to_s, '%m/%d/%Y') if params[:end].present?
  end

  def edit
  end

  def create
    @event = Event.new(event_params)
    
    @event.created_by_id = current_user.id
    @event.updated_by_id = current_user.id

    @event.save
    save_mobile_units
    save_employees
    save_exams
    save_dates
  end

  def update
    # event_params = event_params.merge({
    #   updated_by_id: current_user.id
    # })
    @event.update(event_params)
    @event.updated_by_id = current_user.id
    @event.save 
    save_mobile_units
    save_employees
    save_exams
    save_dates
  end

  def destroy
    # @event.destroy
    @event.status = 0
    @event.updated_by_id = current_user.id
    @event.save
    Rails.logger.info "=====>>> @event.errors: #{@event.errors.full_messages}"
  end

  def add_mobile_unit
    @mobile_unit = MobileUnit.find(params[:mobile_unit_id])
  end
  def remove_mobile_unit
    @mobile_unit = MobileUnit.find(params[:mobile_unit_id])
  end

  def add_employee
    @employee = Employee.find(params[:employee_id])
  end
  def remove_employee
    @employee = Employee.find(params[:employee_id])
  end

  def add_exam
    @exam = Exam.find(params[:exam_id])
  end
  def remove_exam
    @exam = Exam.find(params[:exam_id])
  end

  def get_mobile_units
    all_event_mobile_units = @event.event_mobile_units
    @cars = @event.event_mobile_units.where(:mobile_unit_class => "Car")
    @vans = @event.event_mobile_units.where(:mobile_unit_class => "Van")
    @trucks = @event.event_mobile_units.where(:mobile_unit_class => "Truck")
  end

  def get_employees
    all_event_employees = @event.event_employees
    @staffs = @event.event_employees.where(:employee_class => "Staff").order(:created_at)
    @doctors = @event.event_employees.where(:employee_class => "Doctor").order(:created_at)
  end

  def get_exams
    @exams = @event.event_exams.order(:created_at)
  end

  def save_mobile_units
    if params[:event][:mobile_units].present?
      event_mobile_unit_ids = []
      params[:event][:mobile_units].each do |mobile_unit|
        Rails.logger.info "===>>>mobile_unit#{mobile_unit}"
        if mobile_unit[:id].present?
          event_mobile_unit = EventMobileUnit.find(mobile_unit[:id])
        else
          event_mobile_unit = EventMobileUnit.new
        end
        event_mobile_unit.mobile_unit_class = mobile_unit[:mobile_unit_class]
        event_mobile_unit.mobile_unit_id = mobile_unit[:mobile_unit_id]
        event_mobile_unit.first_driver_id = mobile_unit[:first_driver_id]
        event_mobile_unit.second_driver_id = mobile_unit[:second_driver_id].present? ? mobile_unit[:second_driver_id] : nil
        event_mobile_unit.event_id = @event.id
        event_mobile_unit.save
        event_mobile_unit_ids << event_mobile_unit.id
      end
      @event.event_mobile_units.map{|e| e.destroy unless event_mobile_unit_ids.include?(e.id) }
    else
      @event.event_mobile_units.map{|e| e.destroy}
    end
  end

  def save_employees
    if params[:event][:employees].present?
      event_employees_ids = []
      params[:event][:employees].each do |employee|
        Rails.logger.info "===>>>employee#{employee}"
        if employee[:id].present?
          event_employee = EventEmployee.find(employee[:id])
        else
          event_employee = EventEmployee.new
        end
        event_employee.employee_class = employee[:employee_class]
        event_employee.employee_id = employee[:employee_id]
        event_employee.position = employee[:position]
        event_employee.event_id = @event.id
        event_employee.save
        event_employees_ids << event_employee.id
      end
      @event.event_employees.map{|e| e.destroy unless event_employees_ids.include?(e.id) }
    else
      @event.event_employees.map{|e| e.destroy}
    end
  end

  def save_exams
    if params[:event][:exams].present?
      event_exams_ids = []
      params[:event][:exams].each do |exam|
        Rails.logger.info "===>>>exam#{exam}"
        if exam[:id].present?
          event_exam = EventExam.find(exam[:id])
        else
          event_exam = EventExam.new
        end
        event_exam.exam_id = exam[:exam_id]
        event_exam.event_id = @event.id
        event_exam.save
        event_exams_ids << event_exam.id
      end
      @event.event_exams.map{|e| e.destroy unless event_exams_ids.include?(e.id) }
    else
      @event.event_exams.map{|e| e.destroy}
    end
  end

  def save_dates
    if params[:event][:dates].present?
      event_dates_ids = []
      params[:event][:dates].each do |date|
        Rails.logger.info "===>>>date: #{date}"
        if date[:id].present?
          event_date = EventDate.find(date[:id])
        else
          event_date = EventDate.new
        end
        event_date.date = date[:date]
        event_date.description = date[:description]
        event_date.event_id = @event.id
        if date[:description].present?
          event_date.save
          event_dates_ids << event_date.id
        else
          event_date.description = "Sin descripción"
          event_date.save
          event_dates_ids << event_date.id
        end
      end
      @event.event_dates.map{|e| e.destroy unless event_dates_ids.include?(e.id) }
    else
      Rails.logger.info "===>>>no dates"
      if params[:event][:move].present?
        Rails.logger.info "===>>> on move"
      else
        Rails.logger.info "===>>> on not move"
        @event.event_dates.map{|e| e.destroy}  
      end
    end
  end

  def send_confirmation_email
    @event = Event.find(params[:event_id])
    EventMailer.event_confirm(@event).deliver_now
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:title, :date_range, :start, :end, :color, :authorization, :description, :status, :company_id, :move, :pdf_id)
      # Rails.logger.info "=>>>>> #{params[:event][:event_items_attributes].inspect}"
      # params[:event][:event_items_attributes].each do |event_item|
      #   Rails.logger.info "=>>>>> #{event_item.inspect}"
      #   Rails.logger.info "=>>>>> #{event_item[:car].to_s}"
      #   # event_item[:car] = Car.find event_item[:car].to_s.to_i if event_item[:car].present?
      #   # event_item[:employee] = Employee.find event_item[:employee].to_s.to_i if event_item[:employee].present?
      # end
    end
end
