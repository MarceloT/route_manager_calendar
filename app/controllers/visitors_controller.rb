class VisitorsController < ApplicationController
    before_action :authenticate_user!
	before_action :get_data, only: [:index]
  def index
    beginning_of_week = Date.today.beginning_of_week
    end_of_week = beginning_of_week.end_of_week
    @weekly_events = Event.where(start: beginning_of_week..end_of_week).or(Event.where(end: beginning_of_week..end_of_week))
    beginning_of_day = Date.today.beginning_of_day
    end_of_day = beginning_of_day.end_of_day
    # @daily_events = Event.where(start: beginning_of_day..end_of_day).or(Event.where(end: beginning_of_day..end_of_day))
    @daily_events = Event.where("events.start <= ? AND ? <= events.end",  Date.today,  Date.today)
  end

  def load_data
  	require 'csv'
    # Clear database
    Rails.application.eager_load!
    ActiveRecord::Base.descendants.each { |c| c.delete_all unless c.to_s == "ActiveRecord::SchemaMigration" || c.to_s == "ApplicationRecord"}
    # ActiveRecord::Base.connection.reset_pk_sequence!("users")
    ActiveRecord::Base.connection.reset_pk_sequence!("exams")
    ActiveRecord::Base.connection.reset_pk_sequence!("companies")
    ActiveRecord::Base.connection.reset_pk_sequence!("employees")
    ActiveRecord::Base.connection.reset_pk_sequence!("event_employees")
    ActiveRecord::Base.connection.reset_pk_sequence!("event_mobile_units")
    ActiveRecord::Base.connection.reset_pk_sequence!("events")
    ActiveRecord::Base.connection.reset_pk_sequence!("mobile_units")
    ActiveRecord::Base.connection.reset_pk_sequence!("pdfs")
  	# Add Companies
    datafile = Rails.root + 'db/empresas.csv'
    CSV.foreach(datafile, headers: true) do |row|
      company = Company.where(:name => row[0]).last
      unless company.present?
        c = Company.new
        c.name = row[0]
        c.ruc = row[1]
        c.direcction = row[2]
        c.sector = row[3]
        c.contact_name = row[4]
        c.contact_email = row[5]
        c.doctor_name = row[6]
        c.doctor_email = row[7]
        c.billing_name = row[8]
        c.billing_email = row[9]
        # c.user_id = row[10]
        c.save
      end
    end
    # Add examenes
    datafile = Rails.root + 'db/examenes2.csv'
    CSV.foreach(datafile, headers: true, ) do |row|
      exam = Exam.where(:code => row[0]).last
      unless exam.present?
        e = Exam.new
        e.code = row[0].to_s
        e.name = row[1].to_s
        e.activated = true
        e.province_value = 0
        e.m_units_value = 0
        e.city_value = 0
        Rails.logger.info "==>> e : #{e.inspect}"
        Rails.logger.info "==>> e.save : #{e.save}"
        Rails.logger.info "==>> e.errors : #{e.errors.full_messages.join(", ")}"

      end
    end
    call_rake("tasks:datos_iniciales")
    redirect_to root_path, alert: "Base de datos cargada"
  end
end
