class User < ApplicationRecord

  def role?(role_name)
    case role_name.to_s
    when "admin"
      return self.role == 1 ? true : false
    when "receptionist"
      return self.role == 2 ? true : false
    when "cashier"
      return self.role == 3 ? true : false
    end
  end
  mount_uploader :avatar, AvatarUploader
  attr_accessor :remove_avatar
  validates_presence_of   :email
  validates_integrity_of  :avatar
  validates_processing_of :avatar
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def full_name
    self.name.present? ? self.name : self.email
  end
end
