# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
    user ||= User.new
    contributables = [Company, Exam, Car, Doctor, Driver, Staff, Truck, Van, Event]
    
    cannot :manage, contributables
    # cannot :manage, Exam
    cannot :manage, User
    # cannot :manage, Company

    # if user.role_id == 1 && user.company_id
    #     can :manage, contributables, company_id: user.company_id
    # end

    if user.role?(:receptionist)
      can :read, contributables  
      can :manage, Company
      can :manage, Event
    end

    if user.role?(:admin)
      can :manage, contributables
      # can :manage, Exam
      can :manage, User
      can :manage, Event
      # can :manage, Company
    end
  end
end
