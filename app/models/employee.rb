class Employee < ApplicationRecord
	validates :name, :lastname, presence: true
	scope :available, -> {where("available = true")}
	scope :order_by_name, -> {order(:lastname)}
	def full_name
		full_name = self.lastname.present? ? "#{self.name} #{self.lastname}" : self.name
		full_name = "#{full_name} (No disponible)" if self.available == false
		full_name
	end

	def full_last_name
		full_name = self.lastname.present? ? "#{self.lastname} #{self.name}" : self.name
		full_name = "#{full_name} (No disponible)" if self.available == false
		full_name
	end

	def full_name_and_phone
		self.phone1.present? ? "#{self.full_name} (#{self.phone1})" : self.full_name
	end

	def available_to_s
		self.available.present? ? "Si" : "No"
	end
end
