class MobileUnit < ApplicationRecord
	validates :plate, :status, presence: true
	belongs_to :driver
	scope :available, -> {where("status = 0")}
	def full_name
		car_name = ""		
		if self.company.present?
			car_name += "#{self.company.capitalize}"
		end
		if self.model.present?
			car_name += " #{self.model.capitalize}"
		end
		car_name = car_name.present? ? "#{self.plate} - #{car_name}" : self.plate
	end

	def full_name_with_diver
		self.driver.present? ? "#{self.full_name} - #{self.driver.full_name}" : self.full_name
	end

	def status_name
		if self.status == 0
			'Disponible'
		elsif self.status == 1
			'Matenimiento'
		elsif self.status == 2
			'Fuera de servicio'
		else
			'Otros'
		end
	end
end
