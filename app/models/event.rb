class Event < ApplicationRecord
  validates :title, presence: true
  has_many :event_mobile_units, foreign_key: "event_id"
  has_many :event_employees, foreign_key: "event_id"
  has_many :event_exams, foreign_key: "event_id"
  has_many :event_dates, foreign_key: "event_id"
  belongs_to :company, foreign_key: "company_id"
  belongs_to :pdf, foreign_key: "pdf_id", optional: true
  belongs_to :created_by, class_name: "User", foreign_key: "created_by_id"
  belongs_to :updated_by, class_name: "User", foreign_key: "updated_by_id"

  attr_accessor :date_range, :move

  def all_day_event?
    self.start == self.start.midnight && self.end == self.end.midnight ? true : false
    # false
  end

  def closed?
    editable = false
    if self.end < Time.now
      editable = true
    end
    editable
  end

  def full_name
    self.company_id.present? ? "#{self.company.name} #{self.title}" : self.title
  end

  def has_exam(exam_id = nil)
    self.event_exams.map{|e| e.exam.id.to_i}.include?(exam_id.to_i)
  end
  #TODO: por mejorar esta consulta igual que examnes funcion: has_exam
 #  def has_car(car_id = nil)
 #  	self.event_mobile_units.where(:mobile_unit_id => car_id, :mobile_unit_class => "Car").first.present?
	# end

 #  def has_van(car_id = nil)
 #    self.event_mobile_units.where(:mobile_unit_id => car_id, :mobile_unit_class => "Van").first.present?
 #  end

 #  def has_truck(car_id = nil)
 #    self.event_mobile_units.where(:mobile_unit_id => car_id, :mobile_unit_class => "Truck").first.present?
 #  end

 #  def has_staff(staff_id = nil)
 #    self.event_employees.where(:employee_id => staff_id, :employee_class => "Staff").first.present?
 #  end

 #  def has_doctor(doctor_id = nil)
 #    self.event_employees.where(:employee_id => doctor_id, :employee_class => "Doctor").first.present?
 #  end

  def has_mobile_unit(mobile_unit_id = nil, class_name = nil)
    if self.event_mobile_units.any?
      self.event_mobile_units.where(:mobile_unit_id => mobile_unit_id, :mobile_unit_class => class_name).exists?
    else
      false
    end
    # self.event_mobile_units.where(:mobile_unit_class => class_name).map{|e| e.mobile_unit_id.id.to_i}.include?(mobile_unit_id.to_i)
  end

  def has_employee(employee_id = nil, class_name = nil)
    if self.event_employees.any?
      self.event_employees.where(:employee_id => employee_id, :employee_class => class_name).exists?
    else
      false
    end
    # self.event_employees.where(:employee_class => class_name).map{|e| e.employee.id.to_i}.include?(employee_id.to_i)
  end

  def has_date(date)
    self.event_dates.where(:date => date).first
  end

  def owner
    owner = self.event_employees.where(:position => 'responsable_del_programa').last
    if owner.present?
      owner.employee.full_name_and_phone
    else
      "Sin asignar"
    end
  end

  def is_item_used(item_id = nil, item_class = nil, start_date = nil, end_date = nil)
    events_formated = []
    if start_date && end_date
      if item_class == "mobile_units"
        events = Event.joins(:event_mobile_units).select(:id, :company_id, :title).where("events.start <= ? AND ? <= events.end AND event_mobile_units.mobile_unit_id = ?", start_date, end_date, item_id)        
        unless self.new_record?
          events = events.where("events.id != ?",  self.id)
        end
      elsif item_class == "employees"
        events = Event.joins(:event_employees).select(:id, :company_id, :title).where("events.start <= ? AND ? <= events.end AND event_employees.employee_id = ?", start_date, end_date, item_id)
        unless self.new_record?
          events = events.where("events.id != ?",  self.id)
        end
      end
      events.each do |event|
        events_formated << [event.id, event.full_name]
      end
    end
    events_formated
  end
end
