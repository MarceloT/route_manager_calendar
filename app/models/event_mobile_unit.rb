class EventMobileUnit < ApplicationRecord
	belongs_to :event
	belongs_to :mobile_unit
	def second_driver
		if self.second_driver_id
			Driver.find(second_driver_id)	
		else
			""
		end
	end
end
