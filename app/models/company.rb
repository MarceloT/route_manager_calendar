class Company < ApplicationRecord
	validates :ruc, :name, presence: true
	scope :order_by_name, -> {order(:name)}

	def full_contact_name
		if self.contact_email.present? && self.contact_name.present?
			"#{self.contact_name} (#{self.contact_email})"
		elsif self.contact_name.present?
			self.contact_name
		else
			"No asignado"
		end
	end

	def full_doctor_name
		if self.doctor_email.present? && self.doctor_name.present?
			"#{self.doctor_name} (#{self.doctor_email})"
		elsif self.doctor_name.present?
			self.doctor_name
		else
			"No asignado"
		end
	end

end
