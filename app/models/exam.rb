class Exam < ApplicationRecord
	validates :code, :name, presence: true
	scope :order_by_name, -> {order(:name)}
	def activated_to_s
		self.activated.present? ? "Si" : "No"
	end

	def full_name
		"#{self.code} - #{self.name}"
	end
end
