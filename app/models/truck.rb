class Truck < MobileUnit
	def get_tecnologia
		tecnologia_text = []
		if self.rx
			tecnologia_text << "Rx"
		end
		if self.flat_panel
			tecnologia_text << "Flat Panel"
		end
		if self.audiometria
			tecnologia_text << "Audiometría"
		end
		if self.consultorio		
			tecnologia_text << "Consultorio"
		end
		tecnologia_text.join(", ")
	end
end
