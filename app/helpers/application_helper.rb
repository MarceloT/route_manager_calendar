module ApplicationHelper
  def include_javascript (file)
    s = " <script type=\"text/javascript\">" + render(:file => file) + "</script>"
    content_for(:head, raw(s))
  end

  def select_name (array, value)
  	name = "N/A"
    array.each do |item|
    	if item.last.to_s == value.to_s
    		name = item.first
    		break
    	end
    end
    name
  end

  def link_to_show(url)
    link_to '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>'.html_safe, url, class: "btn btn-default btn-xs show-icon"
  end

  def link_to_edit(content, url)
    if can? :edit, content
      link_to '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>'.html_safe , url, class: "btn btn-default btn-xs"
    end
  end

  def link_to_delete(url)
    if can? :delete, url
      link_to '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'.html_safe , url, method: :delete, data: { confirm: 'Esta seguro?' }, class: "btn btn-default btn-xs"
    end
  end

end
